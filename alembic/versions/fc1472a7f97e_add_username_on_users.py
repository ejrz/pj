"""add_username_on_users

Revision ID: fc1472a7f97e
Revises: 899eb9764a7b
Create Date: 2022-04-02 13:35:40.255506

"""
from alembic import op
import sqlalchemy as sa
import sqlmodel


# revision identifiers, used by Alembic.
revision = 'fc1472a7f97e'
down_revision = '899eb9764a7b'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('users', sa.Column('username', sqlmodel.sql.sqltypes.AutoString(length=100), nullable=True))



def downgrade():
    op.drop_column('users', 'username')