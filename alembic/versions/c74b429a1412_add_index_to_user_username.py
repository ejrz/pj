"""add_index_to_user_username

Revision ID: c74b429a1412
Revises: fc1472a7f97e
Create Date: 2022-04-03 22:21:38.684407

"""
from alembic import op
import sqlalchemy as sa
import sqlmodel


# revision identifiers, used by Alembic.
revision = 'c74b429a1412'
down_revision = 'fc1472a7f97e'
branch_labels = None
depends_on = None


def upgrade():
    op.create_index(op.f('ix_users_username'), 'users', ['username'], unique=False)

def downgrade():
    op.drop_index(op.f('ix_users_username'), table_name='users')
