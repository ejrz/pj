# PJ - PJ Jest

this project contains the file structure of backend using:

python 3.10.2 + FastAPI + Alembic + SLQAlchemy + sqlmodel (ORM) 

## Usage
To run the server on terminal, make sure you have installed the 
python packages, and you're on the root directory of the project.
```commandline
uvicorn src.main:app --reload
```
Access ```localhost:8000/docs``` to test the endpoints on swagger. 

On run the FastAPI server will check and create database ```pjdb```
if the database do not exist.

## Installation
Install and setup ```PostgreSQL```

Clone the project.

Create python virtual environment
```commandline
python3 -m venv venv
```
Activate the venv for powershell
```.\venv\Scripts\activate.ps1 ```
or simply remove the ```.ps1``` 
extension when using command prompt.


Run on the command prompt to install all the python packages needed in the project, make sure you're on the root directory.
```commandline
pip install -r requirements.txt
``` 


Edit the ```root_directory > db_config.ini``` 
depending on your local machine PostgreSQL setup.

Edit the ```root_directory > alembic.ini``` 
depending on your local machine PostgreSQL setup.

**Note:**

To run alembic migration use this command on cmd or powershell ```alembic upgrade head```

## License
[MIT](https://choosealicense.com/licenses/mit/)