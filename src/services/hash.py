from passlib.context import CryptContext


class Hash:
	__hash_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

	def __init__(self) -> None:
		pass

	async def verify_password(self, pw: str, hpw: str):
		validity = self.__hash_context.verify(pw, hpw)
		return validity


	async def hashed(self, pw: str):
		hash = self.__hash_context.hash(pw)
		return hash