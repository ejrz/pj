from typing import List


def build_json_success_response(data: List, datails: str):
	return { 
		"data": data, 
		"status": "success", 
		"details": datails 
	}

def build_json_failed_response(data: List, datails: str):
	return { 
		"data": data, 
		"status": "failed", 
		"details": datails 
	}