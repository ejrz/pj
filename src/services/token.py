import os, configparser
from jose import JWTError, jwt
from datetime import datetime, timedelta
from fastapi import HTTPException, Security
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

class Token:
	__security = HTTPBearer()
	__project_path = os.path.abspath(os.path.join('prs', os.pardir))
	__config_file = __project_path + '\\config.ini'
	__config_auth = 'auth'
	__token = 'token'
	__algorithm = 'algorithm'
	__expiration = 'expiration'

	__config = configparser.ConfigParser()
	__config.read(__config_file)

	__auth_token = __config[__config_auth][__token]
	__auth_algorithm = __config[__config_auth][__algorithm]
	__auth_expiration = int(__config[__config_auth][__expiration])

	def __init__(self) -> None:
		pass

	
	async def encode_token(self, email: str):
		payload = {
			'exp': datetime.utcnow() + timedelta(days=0, minutes=self.__auth_expiration),
			'iat': datetime.utcnow(),
			'sub': email
		}

		return jwt.encode(payload, self.__auth_token, self.__auth_algorithm)

	
	async def decode_token(self, token):
		expire_signature = HTTPException(
			status_code=401,
			detail='Signature has expired',
			headers={"WWW-Authenticate": "Bearer"}
		)
		invalid_token = HTTPException(
			status_code=401,
			detail='Invalid token',
			headers={'WWW-Authenticate': 'Bearer'}
		)
		
		try:
			payload = jwt.decode(token, self.__auth_token, algorithms=[self.__auth_algorithm])
			return payload['sub']
		except jwt.ExpiredSignatureError:
			raise expire_signature
		except JWTError:
			raise invalid_token


	async def secure(self, auth: HTTPAuthorizationCredentials = Security(__security)):
		return await self.decode_token(auth.credentials)