from fastapi import APIRouter, Depends, HTTPException, status
from sqlmodel import select, and_

from src.initializer import Database
from src.schemas.auth import Auth
from src.models.users import Users
from src.services.hash import Hash
from src.services.token import Token
from src.services.generic_response_builder import build_json_success_response

from datetime import datetime, timedelta


db = Database()
hash_service = Hash()
token_service = Token()
auth_route = APIRouter(
	prefix="/api/auth",
	tags=["Auth"]
)


@auth_route.post("/auth_user")
async def auth_user(auth: Auth):
	_sess = db.session_local()
	with _sess as sess:
		query = select(Users).where(and_(Users.email == auth.email, Users.active == True))
		result = sess.exec(query).first()
		
		if not result:
			raise HTTPException(
				status_code=status.HTTP_400_BAD_REQUEST,
				detail="User not found",
				headers={ "WWW:Authenticate": "Bearser" }
			)
		if not await hash_service.verify_password(auth.pw, result.password):
			raise HTTPException(
				status_code=status.HTTP_401_UNAUTHORIZED,
				detail="Incorrect password",
				headers={ "WWW:Authenticate": "Bearer" }
			)

		if type(result.locked_end_date) is type(datetime.utcnow()):
			raise HTTPException(
				status_code=status.HTTP_401_UNAUTHORIZED,
				detail="User was locked",
				headers={ "WWW:Authenticate": "Bearer" }
			)

		user = { 
			"email": result.email, 
			"username": result.username,
			"role": result.user_role.role,
			"locked_end_date": result.locked_end_date
		}
		token = await token_service.encode_token(result.email)
		
		return { "user": user, "accessToken": token }


@auth_route.post("/lock_user", status_code=status.HTTP_200_OK)
async def lock_user(email: str):
	_sess = db.session_local()
	with _sess as sess:
		query = select(Users).where(and_(Users.email == email, Users.active == True))
		result = sess.exec(query).first()
		result.locked_end_date = datetime.utcnow() + timedelta(days=1)
		sess.add(result)
		sess.commit()
		sess.refresh(result)

		return build_json_success_response(data=result.email, datails="User locked")


@auth_route.post("/unlock_user", status_code=200)
async def unlock_user(email: str):
	_sess = db.session_local()
	with _sess as sess:
		query = select(Users).where(and_(Users.email == email, Users.active == True))
		result = sess.exec(query).first()
		result.locked_end_date = None
		sess.add(result)
		sess.commit()
		sess.refresh(result)

		return build_json_success_response(data=result.email, datails="User unlocked")