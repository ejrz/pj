from os import stat
from typing import List

from sqlmodel import select, and_
from fastapi import APIRouter, Depends, HTTPException, status
from src.initializer import Database
from sqlalchemy import exc

from src.models.users import Users
from src.models.user_roles import UserRoles
from src.schemas.users import UserCreate, UserReadCreated, UserRead, UserWithRolesRead
from src.schemas.user_roles import UserRoleCreate, UserRoleReadCreate

from src.services.hash import Hash
from src.services.token import Token
from src.services.generic_response_builder import build_json_failed_response


db = Database()
hash_service = Hash()
token_service = Token()

user_route = APIRouter(
	prefix="/api/users",
	tags=["Users"]
)


def unauthorized_role():
	return HTTPException(
		status_code=status.HTTP_401_UNAUTHORIZED,
		detail="Unauthorized access the current user role"
	)


async def is_admin(email: str):
	_sess = db.session_local()
	with _sess as sess:
		query = select(Users, UserRoles)\
				.join(UserRoles)\
				.where(and_(Users.email == email, UserRoles.role == "Admin"))
		result = sess.exec(query).scalar()
		if result: return True

	return False


@user_route.get("/")
async def index():
	return {"route": user_route.prefix}


@user_route.get("/get_all", response_model=List[UserRead])
async def get_all(email=Depends(token_service.secure)):
	if not await is_admin(email): raise unauthorized_role()
	_sess = db.session_local()
	with _sess as sess:
		query = select(Users)
		result = sess.exec(query)
		return result.all()


@user_route.get("/get_all_with_role", response_model=List[UserWithRolesRead])
async def get_all_with_role(email=Depends(token_service.secure)):
	if not await is_admin(email): raise unauthorized_role()
	_sess = db.session_local()
	with _sess as sess:
		query = select(Users, UserRoles).join(UserRoles)
		result = sess.exec(query)
		return result.all()


@user_route.get("/get_by_id/{id}", response_model=List[UserRead])
async def get_by_id(id: int, email=Depends(token_service.secure)):
	if not await is_admin(email): raise unauthorized_role()
	_sess = db.session_local()
	with _sess as sess:
		query = select(Users).where(Users.id == id)
		result = sess.exec(query)
		return result.all()


@user_route.get("/get_by_email/{email}", response_model=List[UserRead])
async def get_by_email(email: str, di_email=Depends(token_service.secure)):
	if not await is_admin(di_email): raise unauthorized_role()
	_sess = db.session_local()
	with _sess as sess:
		query = select(Users).where(Users.email == email)
		result = sess.exec(query)
		return result.all()


@user_route.get("/get_by_username/{username}", response_model=List[UserRead])
async def get_by_username(username: str, email=Depends(token_service.secure)):
	if not await is_admin(email): raise unauthorized_role()
	_sess = db.session_local()
	with _sess as sess:
		query = select(Users).where(Users.username == username)
		result = sess.exec(query)
		return result.all()


@user_route.get("/get_all_user_role", response_model=List[UserRoleReadCreate])
async def get_all_user_role(email=Depends(token_service.secure)):
	if not await is_admin(email): raise unauthorized_role()
	_sess = db.session_local()
	with _sess as sess:
		query = select(UserRoles)
		result = sess.exec(query)
		return result.all()


@user_route.post("/insert/user")
async def insert_user(user: UserCreate):
	_sess = db.session_local()
	with _sess as sess:
		try:
			db_user = Users.from_orm(user)
			db_user.password = await hash_service.hashed(user.password)
			sess.add(db_user)
			sess.commit()
			sess.refresh(db_user)
			return db_user
		except exc.IntegrityError:
			sess.rollback()
			return build_json_failed_response(data=[], datails="Email already exists")


@user_route.post("/insert/user_role", response_model=UserRoleCreate)
async def insert_user_role(user_role: UserRoleCreate, email=Depends(token_service.secure)):
	if not await is_admin(email): raise unauthorized_role()
	_sess = db.session_local()
	with _sess as sess:
		db_user_role = UserRoles.from_orm(user_role)
		sess.add(db_user_role)
		sess.commit()
		sess.refresh(db_user_role)
		return db_user_role

