from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from src.initializer import Database
from src.routes.users import user_route
from src.routes.auth import auth_route

app = FastAPI()
app.include_router(user_route)
app.include_router(auth_route)


origins = ["http://localhost:3000"]
app.add_middleware(
	CORSMiddleware,
	allow_origins=origins,
	allow_credentials=True,
	allow_methods=["*"],
	allow_headers=["*"]
)

# initialize database creation.
db = Database()
db.create(True)


@app.get("/", status_code=200)
def index():
	yield {"message": "index"}
