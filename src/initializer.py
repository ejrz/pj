import os, configparser

from sqlalchemy_utils import database_exists, create_database
from sqlmodel import create_engine, Session


class Database:
	__project_path = os.path.abspath(os.path.join('prs', os.pardir))
	__config_file = __project_path + '\\config.ini'
	
	__config_db = 'development'
	__dialect = 'dialect'
	__driver = 'driver'
	__username = 'username'
	__password = 'password'
	__host = 'host'
	__port = 'port'
	__dbname = 'db'
	__instance = None

	config = configparser.ConfigParser()
	config.read(__config_file)

	db_dialect = config[__config_db][__dialect]
	db_driver = config[__config_db][__driver]
	db_username = config[__config_db][__username]
	db_password = config[__config_db][__password]
	db_host = config[__config_db][__host]
	db_port = config[__config_db][__port]
	db_name = config[__config_db][__dbname]
	NGIN_URL = db_dialect + '+' + db_driver + '://' + db_username + ':' + db_password + '@' + db_host + '/' + db_name

	__ngin = None
	__Session = None

	def __init__(self) -> None:
		if Database.__instance is not None:
			raise Exception("Database class was already instantiated")
		else:
			self.__instance = self
			self.__ngin = create_engine(self.NGIN_URL, echo=True)

	def create(self, flag: bool = False) -> None:
		if flag and not database_exists(self.__ngin.url):
			create_database(self.__ngin.url)

	def engine(self):
		return self.__ngin

	def session_local(self) -> Session:
		self.__Session = Session(bind=self.__ngin, autocommit=False, autoflush=False)
		return self.__Session
