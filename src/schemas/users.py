from typing import Optional
from sqlmodel import SQLModel
from datetime import datetime

from src.schemas.user_roles import UserRoleReadCreate

class UserCreate(SQLModel):
	email: str
	password: str
	username: str
	user_role_id: int


class UserReadCreated(SQLModel):
	email: str
	username: str


class UserRead(SQLModel):
	id: int
	email: str
	username: Optional[str]
	active: bool
	created_date: datetime
	last_login_date: Optional[datetime]
	locked_end_date: Optional[datetime]


class UserWithRolesRead(SQLModel):
	Users: UserRead
	UserRoles: UserRoleReadCreate