from typing import Optional
from sqlmodel import SQLModel
from datetime import datetime


class UserRoleCreate(SQLModel):
	role: str


class UserRoleReadCreate(SQLModel):
	id: int
	role: str
