from typing import Optional
from datetime import datetime
from sqlalchemy import table
from sqlmodel import (
	SQLModel,
	Field,
	DateTime
)


class UserToken(SQLModel, table=True):
	__tablename__: str = "user_token"
	id: Optional[int] = Field(default=None, primary_key=True)
	user_id: Optional[int] = Field(foreign_key="users.id", index=True)
	active: bool = Field(default=True)
	created_date: datetime = Field(
		sa_column=DateTime(timezone=False),
		nullable=False,
		default=datetime.utcnow()
	)