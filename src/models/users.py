from typing import Optional
from datetime import datetime
from sqlalchemy import Column, String
from sqlmodel import (
	SQLModel,
	Field,
	Relationship,
	DateTime
)
from src.models.user_roles import UserRoles


class Users(SQLModel, table=True):
	__tablename__: str = "users"
	id: Optional[int] = Field(default=None, primary_key=True)
	email: str = Field(index=True, sa_column=Column('email', String, unique=True))
	password: str = Field(max_length=1024)
	username: str = Field(max_length=100, nullable=True, index=True)
	active: bool = Field(default=True)
	user_role_id: Optional[int] = Field(foreign_key="user_roles.id", index=True)
	created_date: datetime = Field(
		sa_column=DateTime(timezone=False),
		nullable=False,
		default=datetime.utcnow()
	)
	last_login_date: Optional[datetime] = Field(
		sa_column=DateTime(timezone=False),
		nullable=True
	)
	locked_end_date: Optional[datetime] = Field(
		sa_column=DateTime(timezone=False),
		nullable=True
	)

	user_role: Optional[UserRoles] = Relationship(back_populates='user')

