from typing import Optional, List
from datetime import datetime
from sqlmodel import (
	SQLModel,
	Field,
	Relationship,
	DateTime
)
from src.models.users import *


class UserRoles(SQLModel, table=True):
	__tablename__: str = "user_roles"
	id: Optional[int] = Field(default=None, primary_key=True)
	role: str
	active: bool = Field(default=True)
	created_date: datetime = Field(
		sa_column=DateTime(timezone=False),
		nullable=False,
		default=datetime.utcnow()
	)

	user: List["Users"] = Relationship(back_populates='user_role')
